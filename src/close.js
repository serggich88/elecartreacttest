let panes = document.querySelectorAll('.paned__item');

for(let paned__item of panes) {
    paned__item.insertAdjacentHTML("afterbegin", '<button class="remove-button">[x]</button>');
  // кнопка становится первым потомком плитки (pane)
  paned__item.onclick = () => paned__item.remove();
}
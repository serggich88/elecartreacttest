
import React from 'react';
import './App.css';


function App() {
  return (
    <div>
    <div className="preloader">
        <div className="preloader__row">
          <div className="preloader__item"></div>
          <div className="preloader__item"></div>
        </div>
      </div>
    <header className="header flex">
        <nav className="header__nav container">
            <ul className="header__list ul-glob flex">
                <li className="header__item">
                    <a href="./." className="header__link">Блок 1</a>
                </li>
                <li className="header__item">
                    <a href="./." className="header__link">Блок 2</a>
                </li>
                <li className="header__item">
                    <a href="./." className="header__link">Блок 3</a>
                </li>
            </ul>
        </nav>
    </header>
    <div className='container'>
        <ul className='container__radio ul-glob flex'>
            <li className='container__radio-item'>
                <lable>
                    <input type='radio' className='container__radio-input' checked></input>
                    <span className='container__radio-chk'>Сортировка по категории</span>
                </lable>
            </li>
            <li className='container__radio-item'>
                <lable>
                    <input type='radio' className='container__radio-input'></input>
                    <span className='container__radio-chk'>Сортировка по названию файла</span>
                </lable>
            </li>
            <li className='container__radio-item'>
                <lable>
                    <input type='radio' className='container__radio-input'></input>
                    <span className='container__radio-chk'>Сортировка по дате</span>
                </lable>
            </li>
            <li className='container__radio-item'>
                <lable>
                    <input type='radio' className='container__radio-input'></input>
                    <span className='container__radio-chk'>Сортровка по размеру файла</span>
                </lable>
            </li>
        </ul>
        <button onClick="location.reload(); return false;">
            Вернуть закрытые карточки
        </button>

        <input type="radio" name="radio" id="one" checked />
        <span>Обычный</span>


        <input type="radio" name="radio" id="two" />
        <span>Древовидный</span>

        <div id="super" className="super__class flex">
            <div>
                <h1 className='content__h1'>Новости</h1>
            </div>
            <ul className='content__list ul-glob flex'>
                <li className='paned__item'>
                    <div className='content__background-1'></div>
                    <h2 className='content__h2'>Карточка № 1</h2>
                </li>
                <li className='paned__item'>
                    <div className='content__background-1'></div>
                    <h2 className='content__h2'>Карточка № 2</h2>
                </li>
                <li className='paned__item'>
                    <div className='content__background-2'></div>
                    <h2 className='content__h2'>Карточка № 3</h2>
                </li>
                <li className='paned__item'>
                    <div className='content__background-2'></div>
                    <h2 className='content__h2'>Карточка № 4</h2>
                </li>
                <li className='paned__item'>
                    <div className='content__background-1'></div>
                    <h2 className='content__h2'>Карточка № 5</h2>
                </li>
                <li className='paned__item'>
                    <div className='content__background-1'></div>
                    <h2 className='content__h2'>Карточка № 6</h2>
                </li>
                <li className='paned__item'>
                    <div className='content__background-2'></div>
                    <h2 className='content__h2'>Карточка № 7</h2>
                </li>
                <li className='paned__item'>
                    <div className='content__background-2'></div>
                    <h2 className='content__h2'>Карточка № 8</h2>
                </li>
                <li className='paned__item'>
                    <div className='content__background-2'></div>
                    <h2 className='content__h2'>Карточка № 9</h2>
                </li>
            </ul>
        </div>
        <div id="collapse" className="panel-collapse collapse in">
            <div className="tree ">
                <ul>
                    <li> <span><i className="fa fa-folder-open"></i> Менюшка</span>
                        <ul>className
                            <li> <span><i className="fa fa-minus-square"></i> другая Менюшка</span>
                                <ul>
                                    <li> <h1 className='content__h1'>Новости</h1>
                                        <ul className='content__list ul-glob flex'>
                                            <li className='paned__item'>
                                                <div className='content__background-1'></div>
                                                <h2 className='content__h2'>Карточка № 1</h2>
                                            </li>
                                            <li className='paned__item'>
                                                <div className='content__background-1'></div>
                                                <h2 className='content__h2'>Карточка № 2</h2>
                                            </li>
                                            <li className='paned__item'>
                                                <div className='content__background-2'></div>
                                                <h2 className='content__h2'>Карточка № 3</h2>
                                            </li>
                                            <li className='paned__item'>
                                                <div className='content__background-2'></div>
                                                <h2 className='content__h2'>Карточка № 4</h2>
                                            </li>
                                            <li className='paned__item'>
                                                <div className='content__background-1'></div>
                                                <h2 className='content__h2'>Карточка № 5</h2>
                                            </li>
                                            <li className='paned__item'>
                                                <div className='content__background-1'></div>
                                                <h2 className='content__h2'>Карточка № 6</h2>
                                            </li>
                                            <li className='paned__item'>
                                                <div className='content__background-2'></div>
                                                <h2 className='content__h2'>Карточка № 7</h2>
                                            </li>
                                            <li className='paned__item'>
                                                <div className='content__background-2'></div>
                                                <h2 className='content__h2'>Карточка № 8</h2>
                                            </li>
                                            <li className='paned__item'>
                                                <div className='content__background-2'></div>
                                                <h2 className='content__h2'>Карточка № 9</h2>
                                            </li>
                                        </ul> </li>
                                </ul>
                            </li>
                            <li> <span><i className="fa fa-minus-square"></i> другая </span>
                                <ul>
                                    <li> <span> Менюшка </span></li>
                                    <li> <span><i className="fa fa-minus-square"></i> Менюшка</span>
                                        <ul>
                                            <li> <span><i className="fa fa-minus-square"></i> Менюшка</span>
                                                <ul>
                                                    <li> <span> Менюшка</span></li>
                                                    <li> <span> Менюшка</span></li>
                                                </ul>
                                            </li>
                                            <li> <span> Менюшка</span> </li>
                                            <li> <span> Менюшка</span> </li>
                                        </ul>
                                    </li>
                                    <li> <span> Менюшка</span></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li> <span><i className="fa fa-folder-open"></i> Менюшка2</span>
                        <ul>
                            <li> <span> Менюшка</span> </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <footer className="footer container">
        <ul className="footer__list flex ul-glob">
            <li className="footer__item">Блок №1</li>
            <li className="footer__item">Блок №2</li>
            <li className="footer__item">Блок №3</li>
            <li className="footer__item">Блок №4</li>
            <li className="footer__item">Блок №5</li>
            <li className="footer__item">Блок №6</li>
        </ul>
    </footer>
    </div>
    
  );
}

export default App;
